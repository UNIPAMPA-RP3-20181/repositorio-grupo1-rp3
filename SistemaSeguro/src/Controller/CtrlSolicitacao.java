/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.*;
import Model.Residencia;

/**
 *
 * @author dee
 */
public class CtrlSolicitacao {

    private SolicitacaoSeguroDAO solicitacaoDAO;
  

    public CtrlSolicitacao(SolicitacaoSeguroDAO solicitacaoDAO) {
        this.solicitacaoDAO = solicitacaoDAO;
    }

    public CtrlSolicitacao() {
        solicitacaoDAO = new SolicitacaoSeguroDAO();
    }

    public double calcularValorSeguro(String uf, int qtdComodos, double areaConstruida, int localizacaoPerigosa, int terrenoPerigoso, int estruturaAmeacada) {
        double valorInicial = 0;
        double valorCalculado = 0;
        double porcento = 0;
        if (uf.equalsIgnoreCase("rs") || uf.equalsIgnoreCase("sc") || uf.equalsIgnoreCase("pr")) {
            valorInicial = 346;
            porcento = valorInicial / 100;

            valorCalculado = valorInicial + (qtdComodos * (porcento * 3));
            valorCalculado = valorCalculado + (areaConstruida * 8.54);
            valorCalculado = valorCalculado + (localizacaoPerigosa * 10);
            valorCalculado = valorCalculado + (terrenoPerigoso * 10);
            valorCalculado = valorCalculado + (estruturaAmeacada * 10);
        } else {
            if (uf.equalsIgnoreCase("sp") || uf.equalsIgnoreCase("rj") || uf.equalsIgnoreCase("mg") || uf.equalsIgnoreCase("es")) {
                valorInicial = 394.80;
                porcento = valorInicial / 100;

                valorCalculado = valorInicial + (qtdComodos * (porcento * 3));
                valorCalculado = valorCalculado + (areaConstruida * 8.54);
                valorCalculado = valorCalculado + (localizacaoPerigosa * 10);
                valorCalculado = valorCalculado + (terrenoPerigoso * 10);
                valorCalculado = valorCalculado + (estruturaAmeacada * 10);
            } else {
                if (uf.equalsIgnoreCase("mt") || uf.equalsIgnoreCase("ms") || uf.equalsIgnoreCase("go")) {
                    valorInicial = 341.55;
                    porcento = valorInicial / 100;

                    valorCalculado = valorInicial + (qtdComodos * (porcento * 3));
                    valorCalculado = valorCalculado + (areaConstruida * 8.54);
                    valorCalculado = valorCalculado + (localizacaoPerigosa * 10);
                    valorCalculado = valorCalculado + (terrenoPerigoso * 10);
                    valorCalculado = valorCalculado + (estruturaAmeacada * 10);
                } else {
                    if (uf.equalsIgnoreCase("ac") || uf.equalsIgnoreCase("am") || uf.equalsIgnoreCase("rr") || uf.equalsIgnoreCase("pa") || uf.equalsIgnoreCase("ro") || uf.equalsIgnoreCase("to") || uf.equalsIgnoreCase("ap")) {
                        valorInicial = 302;
                        porcento = valorInicial / 100;

                        valorCalculado = valorInicial + (qtdComodos * (porcento * 3));
                        valorCalculado = valorCalculado + (areaConstruida * 8.54);
                        valorCalculado = valorCalculado + (localizacaoPerigosa * 10);
                        valorCalculado = valorCalculado + (terrenoPerigoso * 10);
                        valorCalculado = valorCalculado + (estruturaAmeacada * 10);
                    } else {
                        if (uf.equalsIgnoreCase("ma") || uf.equalsIgnoreCase("pi") || uf.equalsIgnoreCase("ce") || uf.equalsIgnoreCase("rn") || uf.equalsIgnoreCase("pb") || uf.equalsIgnoreCase("pe") || uf.equalsIgnoreCase("al") || uf.equalsIgnoreCase("se") || uf.equalsIgnoreCase("ba")) {
                            valorInicial = 327.90;
                            porcento = valorInicial / 100;

                            valorCalculado = valorInicial + (qtdComodos * (porcento * 3));
                            valorCalculado = valorCalculado + (areaConstruida * 8.54);
                            valorCalculado = valorCalculado + (localizacaoPerigosa * 10);
                            valorCalculado = valorCalculado + (terrenoPerigoso * 10);
                            valorCalculado = valorCalculado + (estruturaAmeacada * 10);
                        }
                    }
                }

            }
        }

        return valorCalculado;
    }

    public boolean enviarSolicitacao(int localizacaoPerigosa, int terrenoPerigoso, int estruturaAmeacada, String dataSolicitacao, String dataVisitaResidecia, boolean aprovadaSolicitacao, String motivoReprovacao, String alteracoesSolicitacao, double valorCorrigidoSolicitacao, double valorInicialSolicitacao, int idCliente, int idResidencia) {
       return solicitacaoDAO.insert(localizacaoPerigosa, terrenoPerigoso, estruturaAmeacada, dataSolicitacao, dataVisitaResidecia, aprovadaSolicitacao, motivoReprovacao, alteracoesSolicitacao, valorCorrigidoSolicitacao, valorInicialSolicitacao, idCliente, idResidencia);
    }
    
    public Residencia select(String descricao, int proprietario){
        Residencia resi = new Residencia();
        return resi.select(descricao, proprietario);
    }

}
