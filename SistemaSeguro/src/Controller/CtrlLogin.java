/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;

/**
 *
 * @author dee
 */
public class CtrlLogin {
    private Corretor corretor;
    private Cliente cliente;

    public CtrlLogin(Corretor corretor, Cliente cliente) {
        this.corretor = corretor;
        this.cliente = cliente;
    }
    public CtrlLogin(){
        
    }
 
    public Cliente loginCliente(String login, String senha){
       cliente = new Cliente();
       return cliente.login(login, senha);    
    }
    public Corretor loginCorretor(String login, String senha){
        corretor = new Corretor();
        return corretor.login(login, senha);
        
    }
    
}
