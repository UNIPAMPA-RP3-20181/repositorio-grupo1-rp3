/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;

/**
 *
 * @author dee
 */
public class CtrlCadastro {
    private Residencia residencia;
    private Cliente cliente;
    private Corretor corretor;
    
    public boolean cadastrarCliente(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String dataNascimento, String sexo){
        cliente = new Cliente();
        return cliente.inserir(telefone, email, login, cpf, senha, nome, cep, rua, bairro, cidade, uf, numero, dataNascimento, sexo);
        
    }
    public boolean cadastrarCorretor(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String ativoCorretor, String dataContratacao){
        corretor = new Corretor();
        return corretor.inserir(telefone, email, login, cpf, senha, nome, cep, rua, bairro, cidade, uf, numero, ativoCorretor, dataContratacao);
        
    }
   
    public boolean cadastrarResidencia(String descricao, int qtdComodos, int qtdBanheiros, int qtdGaragens, double areaConstruida, double areaTotal, int numeroAndares, int anoConstrucao, String cep, String rua, String bairro, String cidade, String uf, int numero, int proprietario){
        residencia = new Residencia();
        
        return residencia.inserir(descricao, qtdComodos, qtdBanheiros, qtdGaragens, areaConstruida, areaTotal, numeroAndares, anoConstrucao, cep, rua, bairro, cidade, uf, numero, proprietario);
    }
    
}
