/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.*;

/**
 *
 * @author dee
 */
public abstract class Conexao {

    protected static final int MYDB = 0;
    protected static final String Driver = "org.postgresql.Driver";
    protected Connection con;
    protected Statement stm;
    protected ResultSet rs;

    /**
     * Método que realiza a conexão com o banco de dados.
     *
     * @param banco
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection conexao(int banco) throws ClassNotFoundException, SQLException {

        String url = "jdbc:postgresql://localhost:5432/seguro";
        String usuario = "postgres";
        String senha = "postgres";

        switch (banco) {
            case MYDB:
                Class.forName(Driver);
                break;
        }
        return DriverManager.getConnection(url, usuario, senha);
    }

    /**
     * Método que abre a conexão com o banco de dados.
     */
    protected void conectar() throws SQLException {
        try {
            con = Conexao.conexao(Conexao.MYDB);
            stm = con.createStatement();

        } catch (ClassNotFoundException e) {
            System.out.println("Erro ao carregar o driver");
        }
    }

    /**
     * Método que fecha a conexão com o banco de dados.
     */
    protected void fechar() {
        try {
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Erro ao fechar conexão");
        }
    }
}
