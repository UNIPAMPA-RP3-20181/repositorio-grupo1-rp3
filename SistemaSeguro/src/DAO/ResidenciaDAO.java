/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Residencia;
import java.sql.SQLException;

/**
 *
 * @author dee
 */
public class ResidenciaDAO extends Conexao {

    public Residencia select(String descricao, int proprietario) {
        try {
            conectar();
            String sql = "SELECT * FROM residencia WHERE descricao = '" + descricao + "' AND proprietario = " + proprietario + "";
            rs = stm.executeQuery(sql);
            Residencia residencia = null;
            while (rs.next()) {
                residencia = new Residencia();
                residencia.setIdResidencia(rs.getInt("idResidencia"));
                residencia.setAnoConstrucao(rs.getInt("anoConstrucao"));
                residencia.setAreaConstruida(rs.getInt("areaConstruida"));
                residencia.setAreaTotal(rs.getInt("areaTotal"));
                residencia.setBairro(rs.getString("bairro"));
                residencia.setCep(rs.getString("cep"));
                residencia.setCidade(rs.getString("cidade"));
                residencia.setDescricao(rs.getString("descricao"));
                residencia.setNumero(rs.getInt("numero"));
                residencia.setNumeroAndares(rs.getInt("numeroAndares"));
                residencia.setQtdBanheiros(rs.getInt("qtdBanheiros"));
                residencia.setQtdComodos(rs.getInt("qtdComodos"));
                residencia.setQtdGaragens(rs.getInt("qtdGaragens"));
                residencia.setRua(rs.getString("rua"));
                residencia.setUf(rs.getString("uf"));
            }
            fechar();
            return residencia;
        } catch (SQLException e) {
            System.out.println("Erro com o banco de dados "+e.getMessage());
            fechar();
            return null;
        }

    }

    public boolean insert(String descricao, int qtdComodos, int qtdBanheiros, int qtdGaragens, double areaConstruida, double areaTotal, int numeroAndares, int anoConstrucao, String cep, String rua, String bairro, String cidade, String uf, int numero, int proprietario) {
        try {
            conectar();
            String sql = "INSERT INTO residencia(descricao, \"qtdComodos\", \"qtdBanheiros\", \"qtdGaragens\", \n"
                    + "            \"areaConstruida\", \"areaTotal\", \"numeroAndares\", \"anoConstrucao\", \n"
                    + "            cep, rua, bairro, cidade, uf, numero, proprietario)\n"
                    + "    VALUES ('"+descricao+"',"+qtdComodos+","+qtdBanheiros+","+qtdGaragens+","+areaConstruida+","+areaTotal+","+numeroAndares+","+anoConstrucao+",'"+cep+"','"+rua+"','"+bairro+"','"+cidade+"','"+uf+"',"+numero+","+proprietario+")";
            stm.executeUpdate(sql);
            fechar();
            return true;
        } catch (SQLException e) {
            fechar();
            return false;
        }

    }
}
