/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.SolicitacaoSeguro;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author dee
 */
public class SolicitacaoSeguroDAO extends Conexao {

    public ArrayList<SolicitacaoSeguro> select() {
        ArrayList<SolicitacaoSeguro> listaSolicitacoes = new ArrayList<>();
        try{
            conectar();
            String sql = "SELECT * FROM solicitacao";
            rs = stm.executeQuery(sql);
            while(rs.next()){
                            SolicitacaoSeguro solicitacao = new SolicitacaoSeguro();

                solicitacao.setAlteracoesSolicitacao(rs.getString("alteracoes"));
                solicitacao.setAprovadaSolicitacao(rs.getBoolean("aprovadaSolicitacao"));
                solicitacao.setDataSolicitacao(rs.getString("dataSolicitada"));
                solicitacao.setDataVisitaResidecia(rs.getString("dataVisita"));
                solicitacao.setEstruturaAmeacada(rs.getInt("estruturaAmeacada"));
                solicitacao.setLocalizacaoPerigosa(rs.getInt("localizacaoPerigosa"));
                solicitacao.setMotivoReprovacao(rs.getString("motivoReprovacao"));
                solicitacao.setTerrenoPerigoso(rs.getInt("terrenoPerigoso"));
                solicitacao.setValorCorrigidoSolicitacao(rs.getDouble("valorCorrigido"));
                solicitacao.setValorInicialSolicitacao(rs.getDouble("valorInicial"));
                listaSolicitacoes.add(solicitacao);
                System.out.println(listaSolicitacoes.size());
            }
            fechar();
            return listaSolicitacoes;
        }catch(SQLException e){
            System.out.println(e.getMessage());
            fechar();
            return null;
        }
    }

    public boolean insert(int localizacaoPerigosa, int terrenoPerigoso, int estruturaAmeacada, String dataSolicitacao, String dataVisitaResidecia, boolean aprovadaSolicitacao, String motivoReprovacao, String alteracoesSolicitacao, double valorCorrigidoSolicitacao, double valorInicialSolicitacao, int idCliente, int idResidencia) {
        try {
            conectar();
            String sql = "INSERT INTO solicitacao(\"localizacaoPerigosa\", \"terrenoPerigoso\", \"estruturaAmeacada\", \n"
                    + "            \"dataSolicitada\", \"dataVisita\", \"aprovadaSolicitacao\", \"motivoReprovacao\", \n"
                    + "            alteracoes, \"valorCorrigido\", \"valorInicial\", cliente, residencia, \n"
                    + "            corretor)\n"
                    + "    VALUES ("+localizacaoPerigosa+","+terrenoPerigoso+","+estruturaAmeacada+",'"+dataSolicitacao+"','" +dataVisitaResidecia+"',"+aprovadaSolicitacao+",'"+motivoReprovacao+"','"+alteracoesSolicitacao+"',"+valorCorrigidoSolicitacao+","+valorInicialSolicitacao+","+idCliente+","+idResidencia+","+null+")";
            stm.executeUpdate(sql);
            fechar();
            return true;
        } catch (SQLException e) {
            System.out.print(e.getMessage());
            fechar();
            return false;
        }

    }

}
