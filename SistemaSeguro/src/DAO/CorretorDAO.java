/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Corretor;
import java.sql.*;

/**
 *
 * @author dee
 */
public class CorretorDAO extends Conexao {

    public Corretor select(String login, String senha) {
        try {
            conectar();
            String sql = "SELECT * FROM corretor WHERE login = '" + login + "' AND senha = '" + senha + "'";
            rs = stm.executeQuery(sql);
            Corretor corretor = null;
            while (rs.next()) {
                corretor = new Corretor();
                corretor.setIdCorretor(rs.getInt("idCorretor"));
                corretor.setAtivoCorretor(rs.getString("ativo"));
                corretor.setBairro(rs.getString("bairro"));
                corretor.setCep(rs.getString("cep"));
                corretor.setCidade(rs.getString("cidade"));
                corretor.setCpf(rs.getString("cpf"));
                corretor.setDataContratacao(rs.getString("dataContratacao"));
                corretor.setEmail(rs.getString("email"));
                corretor.setLogin(rs.getString("login"));
                corretor.setNome(rs.getString("nome"));
                corretor.setNumero(rs.getInt("numero"));
                corretor.setRua(rs.getString("rua"));
                corretor.setSenha(rs.getString("senha"));
                corretor.setTelefone(rs.getString("telefone"));
                corretor.setUf(rs.getString("uf"));
            }
            fechar();
            return corretor;
        } catch (SQLException e) {
            System.out.println("Erro com o banco de dados");
            fechar();
            return null;
        }
    }

    public boolean insert(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String dataContratacao, String ativo) {
        try {
            conectar();
            String sql = "INSERT INTO corretor (login, senha, cpf, telefone, email, nome, rua, bairro, cidade, uf, numero, \"dataContratacao\", ativo, cep)VALUES ('" + login + "','" + senha + "','" + cpf + "','" + telefone + "','" + email + "','" + nome + "','" + rua + "','" + bairro + "','" + cidade + "','" + uf + "'," + numero + ",'" + dataContratacao + "','" + ativo + "','" + cep + "')";

            stm.executeUpdate(sql);

            Corretor corretor = new Corretor();
            corretor.setAtivoCorretor(ativo);
            corretor.setBairro(bairro);
            corretor.setCep(cep);
            corretor.setCidade(cidade);
            corretor.setCpf(cpf);
            corretor.setDataContratacao(dataContratacao);
            corretor.setEmail(email);
            corretor.setLogin(login);
            corretor.setNome(nome);
            corretor.setNumero(numero);
            corretor.setRua(rua);
            corretor.setSenha(senha);
            corretor.setTelefone(telefone);
            corretor.setUf(uf);
            fechar();
            return true;
        } catch (SQLException e) {
            fechar();
            System.out.println("Algo deu errado");
            return false;
        }
    }
}
