/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Cliente;
import Model.SolicitacaoSeguro;
import java.sql.*;

/**
 *
 * @author dee
 */
public class ClienteDAO extends Conexao {

    public Cliente select(String login, String senha) {
        try {
            conectar();
            
            String sql = "SELECT * FROM cliente WHERE login = '" + login + "' AND senha = '" + senha + "'";
            rs = stm.executeQuery(sql);
           
            Cliente cliente = null;
            while (rs.next()) {
                cliente = new Cliente();
                cliente.setIdCliente(rs.getInt("idCliente"));
                cliente.setBairro(rs.getString("bairro"));
                cliente.setCep(rs.getString("cep"));
                cliente.setCidade(rs.getString("cidade"));
                cliente.setCpf(rs.getString("cpf"));
                cliente.setDataNascimento(rs.getString("dataNascimento"));
                cliente.setEmail(rs.getString("email"));
                cliente.setLogin(rs.getString("login"));
                cliente.setNome(rs.getString("nome"));
                cliente.setNumero(rs.getInt("numero"));
                cliente.setRua(rs.getString("rua"));
                cliente.setSenha(rs.getString("senha"));
                cliente.setSexo(rs.getString("sexo"));
                cliente.setTelefone(rs.getString("telefone"));
                cliente.setUf(rs.getString("uf"));
            }
            fechar();
            return cliente;
            
        } catch (SQLException e) {
            System.out.println("Erro com o banco de dados");
            fechar();
            return null;
        }

    }

    public String selectNome(int id){
        try{
            conectar();
            String solicitante = "";
            String sql = "SELECT nome FROM cliente WHERE idCliente = "+id;
            rs = stm.executeQuery(sql);
            SolicitacaoSeguro soliSeguro = null; 
            while(rs.next()){
                soliSeguro = new SolicitacaoSeguro();
                soliSeguro.setNomeCliente(rs.getString("nome"));
                solicitante = rs.getString("nome");
            }
            fechar();
            return solicitante;
        }catch(SQLException e){
            System.out.println(e.getMessage());
            fechar();
        }
        return null;
    }
    public boolean insert(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String dataNascimento, String sexo) {
        try {
            conectar();
            String sql = "INSERT INTO cliente(login, senha, cpf, telefone, email, nome, rua, bairro, cidade, uf, numero, \"dataNascimento\", sexo, cep) VALUES ('" + login + "','" + senha + "','" + cpf + "','" + telefone + "','" + email + "','" + nome + "','" + rua + "','" + bairro + "','" + cidade + "','" + uf + "'," + numero + ",'" + dataNascimento + "','" + sexo + "','" + cep + "');";
            stm.executeUpdate(sql);
            Cliente cliente = new Cliente();
            cliente.setBairro(bairro);
            cliente.setCep(cep);
            cliente.setCidade(cidade);
            cliente.setCpf(cpf);
            cliente.setDataNascimento(dataNascimento);
            cliente.setEmail(email);
            cliente.setLogin(login);
            cliente.setNome(nome);
            cliente.setNumero(numero);
            cliente.setRua(rua);
            cliente.setSenha(senha);
            cliente.setSexo(sexo);
            cliente.setTelefone(telefone);
            cliente.setUf(uf);
            fechar();
            return true;
        } catch (SQLException e) {
            fechar();
            System.out.println("Algo deu errado");

            return false;
        }

    }
    public int selectNome(String nome){
        try{
            conectar();
            String sql = "SELECT 'cliente.idCliente' FROM cliente WHERE cliente.nome = '"+nome+"'";
            rs = stm.executeQuery(sql);
            int idProprietario = 0;
            while(rs.next()){
            idProprietario = rs.getInt("cliente.idCliente");
            }
            fechar();
            return idProprietario;
        } catch(SQLException e){
            fechar();
            System.out.println("Cliente não encontrado"+e.getMessage());
            return 0;
        }
    }
}
