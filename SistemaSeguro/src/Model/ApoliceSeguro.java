/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author dee
 */
public class ApoliceSeguro {
    private int numeroApolice;
    private String dataContratacaoApolice;
    private double premioApolice;
    private String nomeCartao;
    private String bandeiraCartao;
    private String vencimentoCartao;
    private int codigoSegurancaCartao;
    private long numeroCartao;

    public ApoliceSeguro(int numeroApolice, String dataContratacaoApolice, double premioApolice, String nomeCartao, String bandeiraCartao, String vencimentoCartao, int codigoSegurancaCartao, long numeroCartao) {
        this.numeroApolice = numeroApolice;
        this.dataContratacaoApolice = dataContratacaoApolice;
        this.premioApolice = premioApolice;
        this.nomeCartao = nomeCartao;
        this.bandeiraCartao = bandeiraCartao;
        this.vencimentoCartao = vencimentoCartao;
        this.codigoSegurancaCartao = codigoSegurancaCartao;
        this.numeroCartao = numeroCartao;
    }

    public int getNumeroApolice() {
        return numeroApolice;
    }

    public void setNumeroApolice(int numeroApolice) {
        this.numeroApolice = numeroApolice;
    }

    public String getDataContratacaoApolice() {
        return dataContratacaoApolice;
    }

    public void setDataContratacaoApolice(String dataContratacaoApolice) {
        this.dataContratacaoApolice = dataContratacaoApolice;
    }

    public double getPremioApolice() {
        return premioApolice;
    }

    public void setPremioApolice(double premioApolice) {
        this.premioApolice = premioApolice;
    }

    public String getNomeCartao() {
        return nomeCartao;
    }

    public void setNomeCartao(String nomeCartao) {
        this.nomeCartao = nomeCartao;
    }

    public String getBandeiraCartao() {
        return bandeiraCartao;
    }

    public void setBandeiraCartao(String bandeiraCartao) {
        this.bandeiraCartao = bandeiraCartao;
    }

    public String getVencimentoCartao() {
        return vencimentoCartao;
    }

    public void setVencimentoCartao(String vencimentoCartao) {
        this.vencimentoCartao = vencimentoCartao;
    }

    public int getCodigoSegurancaCartao() {
        return codigoSegurancaCartao;
    }

    public void setCodigoSegurancaCartao(int codigoSegurancaCartao) {
        this.codigoSegurancaCartao = codigoSegurancaCartao;
    }

    public long getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(long numeroCartao) {
        this.numeroCartao = numeroCartao;
    }
    
    
}
