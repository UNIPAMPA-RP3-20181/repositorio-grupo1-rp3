/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.ClienteDAO;


/**
 *
 * @author dee
 */
public class Cliente extends Usuario{
       private String dataNascimento;
       private String sexo;
       private ClienteDAO clienteDAO;
       private int idCliente;


    public Cliente(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String dataNascimento, String sexo, int idCliente) {
        super(telefone, email, login, cpf, senha, nome, cep, rua, bairro, cidade, uf, numero);
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
    }
    public Cliente(){
        clienteDAO = new ClienteDAO();
    }
  public Cliente login(String login, String senha){
     
      return clienteDAO.select(login, senha);
  }

  public boolean inserir(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String dataNascimento, String sexo){
      return clienteDAO.insert(telefone, email, login, cpf, senha, nome, cep, rua, bairro, cidade, uf, numero, dataNascimento, sexo);
  }

  
    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    
    
}
