/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.ClienteDAO;
import DAO.SolicitacaoSeguroDAO;

/**
 *
 * @author dee
 */
public class SolicitacaoSeguro {
    private int localizacaoPerigosa;
    private int terrenoPerigoso;
    private int estruturaAmeacada;
    private String dataSolicitacao;
    private String dataVisitaResidecia;
    private boolean aprovadaSolicitacao;
    private String motivoReprovacao;
    private String alteracoesSolicitacao;
    private double valorCorrigidoSolicitacao;
    private double valorInicialSolicitacao;
    private SolicitacaoSeguroDAO soliciDAO;
    private int cliente;
    private String clienteNome;

    public SolicitacaoSeguro(int localizacaoPerigosa, int terrenoPerigoso, int estruturaAmeacada, String dataSolicitacao, String dataVisitaResidecia, boolean aprovadaSolicitacao, String motivoReprovacao, String alteracoesSolicitacao, double valorCorrigidoSolicitacao, double valorInicialSolicitacao) {
        this.localizacaoPerigosa = localizacaoPerigosa;
        this.terrenoPerigoso = terrenoPerigoso;
        this.estruturaAmeacada = estruturaAmeacada;
        this.dataSolicitacao = dataSolicitacao;
        this.dataVisitaResidecia = dataVisitaResidecia;
        this.aprovadaSolicitacao = aprovadaSolicitacao;
        this.motivoReprovacao = motivoReprovacao;
        this.alteracoesSolicitacao = alteracoesSolicitacao;
        this.valorCorrigidoSolicitacao = valorCorrigidoSolicitacao;
        this.valorInicialSolicitacao = valorInicialSolicitacao;
    }
    public SolicitacaoSeguro(){
        soliciDAO = new SolicitacaoSeguroDAO();
    }

   public boolean criar(int localizacaoPerigosa, int terrenoPerigoso, int estruturaAmeacada, String dataSolicitacao, String dataVisitaResidecia, boolean aprovadaSolicitacao, String motivoReprovacao, String alteracoesSolicitacao, double valorCorrigidoSolicitacao, double valorInicialSolicitacao, int idCliente, int idResidencia) {
       return soliciDAO.insert(localizacaoPerigosa, terrenoPerigoso, estruturaAmeacada, dataSolicitacao, dataVisitaResidecia, aprovadaSolicitacao, motivoReprovacao, alteracoesSolicitacao, valorCorrigidoSolicitacao, valorInicialSolicitacao, idCliente, idResidencia);
   }

    public int getLocalizacaoPerigosa() {
        return localizacaoPerigosa;
    }

    public void setLocalizacaoPerigosa(int localizacaoPerigosa) {
        this.localizacaoPerigosa = localizacaoPerigosa;
    }

    public int getTerrenoPerigoso() {
        return terrenoPerigoso;
    }

    public void setTerrenoPerigoso(int terrenoPerigoso) {
        this.terrenoPerigoso = terrenoPerigoso;
    }

    public int getEstruturaAmeacada() {
        return estruturaAmeacada;
    }

    public void setEstruturaAmeacada(int estruturaAmeacada) {
        this.estruturaAmeacada = estruturaAmeacada;
    }

    public String getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(String dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public String getDataVisitaResidecia() {
        return dataVisitaResidecia;
    }

    public void setDataVisitaResidecia(String dataVisitaResidecia) {
        this.dataVisitaResidecia = dataVisitaResidecia;
    }

    public boolean isAprovadaSolicitacao() {
        return aprovadaSolicitacao;
    }

    public void setAprovadaSolicitacao(boolean aprovadaSolicitacao) {
        this.aprovadaSolicitacao = aprovadaSolicitacao;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public String getAlteracoesSolicitacao() {
        return alteracoesSolicitacao;
    }

    public void setAlteracoesSolicitacao(String alteracoesSolicitacao) {
        this.alteracoesSolicitacao = alteracoesSolicitacao;
    }

    public double getValorCorrigidoSolicitacao() {
        return valorCorrigidoSolicitacao;
    }

    public void setValorCorrigidoSolicitacao(double valorCorrigidoSolicitacao) {
        this.valorCorrigidoSolicitacao = valorCorrigidoSolicitacao;
    }
     public double getValorInicialSolicitacao() {
        return valorInicialSolicitacao;
    }

    public void setValorInicialSolicitacao(double valorInicialSolicitacao) {
        this.valorInicialSolicitacao = valorInicialSolicitacao;
    }

    public String getNomeCliente(int idCliente) {
        ClienteDAO cDAO = new ClienteDAO();
        return cDAO.selectNome(idCliente);
        
    }
    public void setNomeCliente(String clienteNome){
        this.clienteNome = clienteNome;
    }
    public int getCliente(){
        return cliente;
    }
    public void setCliente(int cliente) {
        this.cliente = cliente;
    }
    
}
