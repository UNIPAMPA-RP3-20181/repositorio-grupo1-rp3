/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.ResidenciaDAO;

/**
 *
 * @author dee
 */
public class Residencia {
    
    private String descricao;
    private int qtdComodos;
    private int qtdBanheiros;
    private int qtdGaragens;
    private double areaConstruida;
    private double areaTotal;
    private int numeroAndares;
    private int anoConstrucao;
    private String cep;
    private String rua;
    private String bairro;
    private String cidade;
    private String uf;
    private int numero;
    private int idResidencia;
    private ResidenciaDAO resiDAO;
    
    public Residencia(String descricao, int qtdComodos, int qtdBanheiros, int qtdGaragens, double areaConstruida, double areaTotal, int numeroAndares, int anoConstrucao, String cep, String rua, String bairro, String cidade, String uf, int numero, int idResidencia) {
        this.descricao = descricao;
        this.qtdComodos = qtdComodos;
        this.qtdBanheiros = qtdBanheiros;
        this.qtdGaragens = qtdGaragens;
        this.areaConstruida = areaConstruida;
        this.areaTotal = areaTotal;
        this.numeroAndares = numeroAndares;
        this.anoConstrucao = anoConstrucao;
        this.cep = cep;
        this.rua = rua;
        this.bairro = bairro;
        this.cidade = cidade;
        this.uf = uf;
        this.numero = numero;
        this.idResidencia = idResidencia;
    }

    public Residencia() {
        resiDAO = new ResidenciaDAO();
    }

    public boolean inserir(String descricao, int qtdComodos, int qtdBanheiros, int qtdGaragens, double areaConstruida, double areaTotal, int numeroAndares, int anoConstrucao, String cep, String rua, String bairro, String cidade, String uf, int numero,int proprietario){
        return resiDAO.insert(descricao, qtdComodos, qtdBanheiros, qtdGaragens, areaConstruida, areaTotal, numeroAndares, anoConstrucao, cep, rua, bairro, cidade, uf, numero, proprietario);
        
    }
    public Residencia select(String descricao, int proprietario){
        return resiDAO.select(descricao, proprietario);
        
    }
    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getQtdComodos() {
        return qtdComodos;
    }

    public void setQtdComodos(int qtdComodos) {
        this.qtdComodos = qtdComodos;
    }

    public int getQtdBanheiros() {
        return qtdBanheiros;
    }

    public void setQtdBanheiros(int qtdBanheiros) {
        this.qtdBanheiros = qtdBanheiros;
    }

    public int getQtdGaragens() {
        return qtdGaragens;
    }

    public void setQtdGaragens(int qtdGaragens) {
        this.qtdGaragens = qtdGaragens;
    }

    public double getAreaConstruida() {
        return areaConstruida;
    }

    public void setAreaConstruida(double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    public double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public int getNumeroAndares() {
        return numeroAndares;
    }

    public void setNumeroAndares(int numeroAndares) {
        this.numeroAndares = numeroAndares;
    }

    public int getAnoConstrucao() {
        return anoConstrucao;
    }

    public void setAnoConstrucao(int anoConstrucao) {
        this.anoConstrucao = anoConstrucao;
    }

    public int getIdResidencia() {
        return idResidencia;
    }

    public void setIdResidencia(int idResidencia) {
        this.idResidencia = idResidencia;
    }
    
}
