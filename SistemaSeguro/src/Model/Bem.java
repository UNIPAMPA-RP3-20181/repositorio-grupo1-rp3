/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author dee
 */
public class Bem {
    private String descBem;
    private double valorEstimadoBem;
    private Residencia residencia;

    public Bem(String descBem, double valorEstimadoBem, Residencia residencia) {
        this.descBem = descBem;
        this.valorEstimadoBem = valorEstimadoBem;
        this.residencia = residencia;
    }

    public String getDescBem() {
        return descBem;
    }

    public void setDescBem(String descBem) {
        this.descBem = descBem;
    }

    public double getValorEstimadoBem() {
        return valorEstimadoBem;
    }

    public void setValorEstimadoBem(double valorEstimadoBem) {
        this.valorEstimadoBem = valorEstimadoBem;
    }

    public Residencia getResidencia() {
        return residencia;
    }

    public void setResidencia(Residencia residencia) {
        this.residencia = residencia;
    }
    
    
    
}
