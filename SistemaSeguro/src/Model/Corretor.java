/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DAO.CorretorDAO;

/**
 *
 * @author dee
 */
public class Corretor extends Usuario{
    private String ativoCorretor;
    private String dataContratacao;
    private CorretorDAO corretorDAO;
    private int idCorretor;
    
    public Corretor(){
        corretorDAO = new CorretorDAO();
        
    }
    public Corretor(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String ativoCorretor, String dataContratacao, int idCorretor) {
        super(telefone, email, login, cpf, senha, nome, cep, rua, bairro, cidade, uf, numero );
        this.ativoCorretor = ativoCorretor;
        this.dataContratacao = dataContratacao;
    }

     public Corretor login(String login, String senha){
        return corretorDAO.select(login, senha);
      
  }
     public boolean inserir(String telefone, String email, String login, String cpf, String senha, String nome, String cep, String rua, String bairro, String cidade, String uf, int numero, String dataContratacao, String ativoCorretor){
        return corretorDAO.insert(telefone, email, login, cpf, senha, nome, cep, rua, bairro, cidade, uf, numero, dataContratacao, ativoCorretor);
         
     }
    
    public String getAtivoCorretor() {
        return ativoCorretor;
    }

    public void setAtivoCorretor(String ativoCorretor) {
        this.ativoCorretor = ativoCorretor;
    }

    public String getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(String dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public int getIdCorretor() {
        return idCorretor;
    }

    public void setIdCorretor(int idCorretor) {
        this.idCorretor = idCorretor;
    }
    
    
    
}
